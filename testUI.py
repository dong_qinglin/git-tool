#!/usr/bin/python
# -*- coding: UTF-8 -*-

from Tkinter import *
'''
为Listbox添加滚动条。 
滚动条是独立的组件。 
为了在某个足尖上安装垂直滚动条，你需要做两件事： 
1、设置该组件的yscrollbarcommand选项为Scrollbar组件的set()方法 
2、设置Scrollbar组件的command选项为该组件的yview()方法 
'''
root = Tk()
#垂直滚动条组件  
slb = Scrollbar(root)
#设置垂直滚动条显示的位置 
slb.pack(side=RIGHT,fill=Y)
#Listbox组件添加Scrollbar组件的set()方法
lb = Listbox(root,yscrollcommand=slb.set)
for i in range(1000):
    lb.insert(END,i)
lb.pack(side=LEFT,fill=BOTH)
bt = Button(root, text='删除', command=lambda x=lb:x.delete())
bt.pack()
#设置Scrollbar组件的command选项为该组件的yview()方法
slb.config(command=lb.yview)
mainloop()
