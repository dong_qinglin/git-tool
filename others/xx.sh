#!/bin/sh
# 作者：Dong
# 邮箱：d173177271@163.com
 clear

 function showMsg()
 {
   echo -e "\033[32m$1\033[0m"
 }
 
 function showCmd()
 {
   echo -e "\033[31m$1\033[0m"
 }

 #杠数
 lstRepo=(


 )
 hainan=(
   #海南地区游戏
   hnmj # 海南麻将
)
 hunan=(
   # 湖南地区 
   mjzz # 转转麻将
   csha # 长沙麻将
   area_hunan # 湖南地区资源
)
 yunnan=(
   # 云南地区
   chux # 楚雄麻将
   linc # 临沧麻将
   yuxi # 玉溪麻将
   qjfj # 曲靖飞小鸡
   bsha # 保山麻将
   yket # 卡二条
)
 guangxi=(
   # 广西地区
   hzmj # 贺州麻将
   nnmj # 南宁麻将
   bhmj # 北海麻将
   gxtd # 广西推倒胡
   gxzz # 桂林转转
)
 neimeng=(
   #内蒙地区
   nmcf #赤峰
   mmxm #锡盟
   nmmq #莫旗
   nmmj #内蒙古
)
 jiangsu=(
   # 江苏地区
   wxdd # 无锡倒到胡
   xuzh # 徐州麻将
   jswx # 无锡麻将
   suzh # 苏州麻将
   gyun # 灌云麻将
   jsgy # 赣榆麻将
   tzkz # 泰州卡子
   tztd # 姜堰麻将
)
 # 改成按照地区建表
 anhui=(
   # 安徽地区
   xiax #萧县
   ahwh #芜湖
   ahtl #铜陵
   ahsz #宿州
   ahma #马鞍山
   ahhf #合肥红中
   czwq #滁州五七番
   cztd #滁州推倒胡
   ahcz #滁州麻将
   chhu #巢湖
   bztd #亳州推倒胡
   ahth #太和麻将
   ahla #六安
   ahhb #淮北
   ahfy #阜阳
   ahch #池州
   ahbb #蚌埠
   ahhs #黄山
   ahys #颍上
   ahaq #安庆麻将
   ahhh #淮南换换
   ahxc #宣城
)
 ningxia=(
   #宁夏
   #宁夏划水
   nxhs
   #宁夏红中
   nxhz
   #固原捉鸟
   gymj 
   #海原麻将
   hymj
   #宁夏一口香
   nxyk
   #石嘴山麻将
   szmj 
   #吴忠麻将
   wzmj 
   #西吉捉鸟
   xjzn 
   #银川麻将
   yinc 
   #中卫麻将
   zwmj 
)
 jiangxi=(
    # 江西
   #四团
   situ
   #二七王
   erqw
   #抚州麻将
   fzmj
   #吉安麻将
   jian
   #江西红中麻将
   jxhz
   #江西转转
   jxzz
   #赣州冲关
   mjgz
   #九江麻将
   mjjj
   #萍乡麻将
   mjpx
   #上饶麻将
   mjsr
   #新余麻将
   mjxy
   #鹰潭麻将
   mjyt
   #景德镇麻将
   mjzd
   #南昌麻将
   ncmj
   #新余五十K
   x5sk
   #宜春麻将
   ycmj
   #宜春万载
   ycwz
   #上顿渡
   sddu
)
 longjiang=(
   # #黑龙江
   area_longjiang
   #哈尔滨
   mjhb
   # #伊春
   mjyc
   # # 双鸭山
   syas
   # # 牡丹江
   mjmd
   # #鹤岗
   mjhg
   # # 泰来
   tlai
   # # 佳木斯
   mjjs
   # #鸡西
   mjjx
   # #嫩江
   nenj
   # # 绥滨
   sbin
   # # 铁力
   tiel
   # #齐齐哈尔麻将
   mjqq
   # # 海林
   hlin
   # #桦 南
   hnan
   # # 北安
   bean
   # # 加格达奇
   jglq
   # #桦川
   huac
   # # 宁安
   mjna
   # # 五大连池
   wdlc
   # # 富锦
   fjin
   # # 七台河
   mjqh
   # #黑河
   mjhh
   # #大庆
   mjdq
   # # 绥化
   mjsh
   # #阿城
   ache
   # #巴彦
   bymj
   # #穆棱
   mjml
   # #肇东
   zhdo
   # #鸡西晃晃
   jxhh
   # #明皇暗宝
   qqbh
   # #宾县麻将
   binx
   # #宝清
   baoq
   # 孙吴麻将
   sunw
)
 guangdong=(
   # 广东地区
   area_guangdong # 广东地区资源包
   gdtd # 广东推倒胡
   gdsw # 广东汕尾
   gdhz # 广东红中王
   gdkj # 客家玩法
   gdjp # 广东鸡平胡
   gdhy # 百搭鸡胡
   mzzz # 梅州转转
   gdjy # 揭阳麻将
   gdcz # 潮州
   gdzz # 惠州庄
   yibz # 100张
   gdmz # 梅州麻将
   gdbh # 惠州庄补花
   gdzj # 湛江麻将
   gdsg # 韶关麻将
   gdqy # 清远100
   gdcs # 潮汕玩法
   gdma # 茂名
   gdyj # 阳江
   gdmh # 梅州红中宝
   gdgg # 江门
   gdqs # 二人雀神
   gdzq # 肇庆
)
 sichuan=(
   area_sichuan # 四川资源
   sctd # 四川推倒胡
   luzh # 泸州麻将
   ayue # 安岳麻将
   sich # 川渝血战
   pzhi # 攀枝花
   zigo # 自贡
   dazh # 达州
   humj # 花麻将
   yaan # 雅安
   lzde # 泸州大二
   erqs # 乐山二七十
   yibi # 宜宾
   many # 绵阳
   lesh # 乐山麻将
   guan # 广安麻将
   lsha # 凉山
   lsdk # 凉山跑得快
   neij # 内江
   deya # 德阳
   meis # 眉山
   lazh # 阆中
 )
 tongyong=(
   #公共游戏
   xlch #血流成河
   ddzh #斗地主
   erdz #二人斗地主
   gady #干瞪眼
   padk #跑得快
   sdyi #三打一
   jtsl #九台甩龙
   xzdd #血战到底
   jlhs #吉林红十
   dbpy #东北刨幺
   gniu #拱牛
   shji #升级
   tdak #心悦踢
 )
 liaoning=(
   # 辽宁游戏
   area_liaoning
   kymt #开原满天飞
   mjfk #法库麻将
   mjzw #彰武
   wafd #瓦房店
   mjxf #西丰
   mjkp #康平
   mjyx #义县
   mjcj #纯夹
   hrmj #桓仁
   lntd #辽宁推倒胡
   mjdt #灯塔
   mjxm #新民
   bxcm #川麻
   lzmj #辽中
   dbsh #调兵山
   lnhh #辽宁晃晃
   mjyk #营口
   wfaz #瓦房镇
   mjbx #本溪
   mjdl #大连
   mjct #昌图
   tamj #台安麻将
   mjly #辽阳麻将
   mjhs #黑山
   mjfx #阜新
   mjdd #丹东
   mjfs #抚顺
   mjhc #海城
   mjcy #朝阳
   mjtl #铁岭
   mjas #鞍山
   mjhl #葫芦岛
   mjsy #沈阳
   lnly #凌源
   pjmj #盘锦
   mjjz #锦州
   mjzh #庄河
   xymj #岫岩
)
 shanxii=(
   # 山西游戏
   czfp # 长治推倒胡
   xnsj # 乡宁摔金
   lfdz # 临汾斗地主
   sxkd # 山西扣点
   sxtd # 山西推倒胡
   daqi # 吕梁打七
   lvkd # 吕梁扣点
   llkd # 捉耗子
   sxxz # 繁峙下雨
   yimp # 一门牌
   dagf # 大同乱刮风
   jzsj # 晋中拐三角
   jinz # 晋中玩法
   gasj # 拐三角玩法
   hznz # 霍州撵中子
   ebja # 1928夹
   tykd # 太原扣点
)
 shanxi=(
   # 陕西麻将
   area_shanxi # 陕西资源
   anka # 安康麻将
   baoj # 宝鸡麻将
   xian # 西安麻将
   hanz # 汉中麻将
   wein # 渭南麻将
   yuli # 榆林麻将
   hozh # 陕西推倒胡
)
 zhejiang=(      
   # 浙江地区游戏
   area_zhejiang # 浙江资源
   quzh # 衢州
   taiz # 台州
)
 hebei=(
   # 河北地区游戏
   area_hebei # 河北通用
   mjsz # 石家庄麻将
   hbtd # 河北推倒胡
   mj59 # 河北红中
   mjhd # 秦皇岛麻将
   lumq # 唐山撸麻巧
   cdmj # 承德麻将
   hsmj # 衡水麻将
   bdmj # 保定麻将
   bdtd # 保定推倒胡
   mjts # 唐山麻将
   xtai # 邢台麻将
   czmj # 沧州麻将
   lfmj # 廊坊麻将
   zjkk # 张家口扣张
   bdtd # 保定推倒胡
   zjkm # 河北碰碰胡
   ywjm # 一五九麻将
   luhd # 秦皇岛
   rqmj # 任丘麻将
)
 hubei=(
   # 湖北地区游戏
   area_hubei #湖北地区资源
   yckw # 宜城卡五星
   xykw # 襄阳卡五星
   xnmj # 咸宁麻将
   xgkw # 孝感卡五星
   tmhh # 天门晃晃
   szkw # 随州卡五星
   qjhh # 潜江晃晃
   jmsk # 荆门双开
   jzmj # 荆州玩法
   jzho # 荆州晃晃
   jmen # 荆门晃晃
   ezho # 鄂州晃晃
   ensh # 恩施麻将
   xtao # 仙桃赖晃
   sykw # 十堰卡五星
   xini # 咸宁晃晃
   hshi # 黄石麻将
   whan # 武汉麻将
   yich # 宜昌血流
)
 tianjin=(
   tjmj # 天津
   area_tianjin # 天津地区资源
   mjxq # 西青麻将
   )

 # 删除多余文件：添加到这里的文件会在遍历时删除
 # 如果你喜欢当然也可以选择添加到打包忽视里但是
 # 那要涉及文件读写，用脚本来写实现起来过于复杂
 # 所以你喜欢可以自己加我就不加了=。=
 listrm=(
 RunScript.bat
 )
 # 地区特殊部分列表
 lstlink=(
   LiaoNing_New                  # 湖北
   HeBei                         # 河北
   ZJGamePro                     # 浙江
   SX/SXGAME_H5                  # 陕西
   ShanXiGroup/SXJSGame          # 山西
   LiaoNing_New                  # 辽宁
   WXCommonGame                  # 通用
   SiChuan/games                 # 四川
   GuangDong/JsGames             # 广东
   HLJJSGame                     # 龙江
   JX/games_html                 # 江西
   NingXia/games_js              # 宁夏
   AnHui                         # 安徽
   jiangsu                       # 江苏
   NeiMeng                       # 内蒙
   GuangXi/js_game               # 广西
   YunNanGroup/YNJSGames         # 云南
   HNSmallGamePro                # 湖南
   LiaoNing_New                  # 海南
   LiaoNing_New                  # 天津
   SiChuan/games                 # 四川
   )
 temp=(
   sich
   )
 # hz=_wallCountReconnect
 # filename=CountGang.js
 # typeset -u qz

 showMsg '注意:这个脚本是由崔哥初版，你需要输入的地区是汉语拼音小写，就像：zhejiang，这样。
 你需要自行确保你输入的分支是正确存在的，除非你希望看到报错信息。毕竟，没有知道分支会起什么鬼名字
 当然也就无从知晓你要获取的分支是否正确，所以确保你输入的正确是你要做的事情。如果出现了错误请立即
 连续按 Ctrl + C 来退出执行这很重要，否则没人知道你的仓库会怎样。其他的意外情况我都已经做了中端进程
 或者跳过循环处理，一般不会出现问题。 '
  showMsg '多说一句，你没准会希望新增一个地区，毕竟谁知道什么时间上新，那么你就需要：\n首先，新增一个
  地区列表同样使用 汉语拼音 去命名。\n然后，你需要在 lstlink 里面补充上他的特殊部分并下面仿写elif部分
  来确保他能正确拼接。 \n最后，如果你有除了RunScript.bat以外的文件要删出去lstrm里添加，不过这里我还没验证过。'

read -p "输入你想要拉取的地区以回车键结束输入（仅接受一个参数） > " area
showMsg ' 你即将拉取的地区是（拼音）： '$area' '
read -p "输入你的目标分支并以回车键结束输入（仅接受一个参数） > " branch
showMsg ' 你即将拉取的分支是： '$branch''

# 安全判断防止分支节点为空，虽然上边你就能看到但假如你不会退出他可以救你
if [[ -z $branch ]];then
   showMsg ' Ops，你可能忘记了输入分支节点再看看吧 '
   read anykey
   exit
fi
# 安全判断防止地区为空，虽然上边也能看到但就是怕你不会
if [ -z "$area" ];then
   showMsg ' Ops，你没有输入地区这样可不行！！ '
   read anykey
   exit
fi

# 这里是超级无趣的重复十几遍根据地区获取中间不同的部分
if [ "$area" = "hubei" ];then
   link=${lstlink[0]}
  showMsg ' Test success '$link''
elif [ "$area" = "hebei" ];then
   link=${lstlink[1]}
  showMsg ' Test success '$link''
elif [ "$area" = "zhejiang" ];then
   link=${lstlink[2]}
  showMsg ' Test success '$link''
elif [ "$area" = "shanxi" ];then
   link=${lstlink[3]}
  showMsg ' Test success '$link''
elif [ "$area" = "shanxii" ];then
   link=${lstlink[4]}
  showMsg ' Test success '$link''
elif [ "$area" = "liaoning" ];then
   link=${lstlink[5]}
  showMsg ' Test success '$link''
elif [ "$area" = "tongyong" ];then
   link=${lstlink[6]}
  showMsg ' Test success '$link''
elif [ "$area" = "sichuan" ];then
   link=${lstlink[7]}
  showMsg ' Test success '$link''
elif [ "$area" = "guangdong" ];then
   link=${lstlink[8]}
  showMsg ' Test success '$link''
elif [ "$area" = "longjiang" ];then
   link=${lstlink[9]}
  showMsg ' Test success '$link''
elif [ "$area" = "jiangxi" ];then
   link=${lstlink[10]}
  showMsg ' Test success '$link''
elif [ "$area" = "ningxia" ];then
   link=${lstlink[11]}
  showMsg ' Test success '$link''
elif [ "$area" = "anhui" ];then
   link=${lstlink[12]}
  showMsg ' Test success '$link''
elif [ "$area" = "jiangsu" ];then
   link=${lstlink[13]}
  showMsg ' Test success '$link''
elif [ "$area" = "neimeng" ];then
   link=${lstlink[14]}
  showMsg ' Test success '$link''
elif [ "$area" = "guangxi" ];then
   link=${lstlink[15]}
  showMsg ' Test success '$link''
elif [ "$area" = "yunnan" ];then
   link=${lstlink[16]}
  showMsg ' Test success '$link''
elif [ "$area" = "hunan" ];then
   link=${lstlink[17]}
  showMsg ' Test success '$link''
elif [ "$area" = "hainan" ];then
   link=${lstlink[18]}
  showMsg ' Test success '$link''
elif [ "$area" = "tianjin" ];then
   link=${lstlink[19]}
  showMsg ' Test success '$link''
elif [ "$area" = "temp" ];then
   link=${lstlink[20]}
  showMsg ' Test success '$link''

else
  showMsg ' 你输入的地区没有对应的git链接，看看是拼音没写对还是说你该看看怎么添加了 ' 
  read anykey
  exit  
fi
# 读取数组长度赋值进行循环
eval 'length="${'"#$area[*]"'}"'

 # 开关控制基本命令
 # 是否需要清理未受版本控制文件并重置到HEAD节点(Y)不需要就注掉

 # for repo in ${list[@]}
 for ((i=0; i<$length; i++)); do
   # 将repo重新赋值为列表对应游戏短名
   eval 'repo=${'${area}'['$i']}'

   # 仓库链接在这拼好不然要改好多地方
   gitlink=git@192.168.67.10:${link}/${repo}.git

   # 本地仓库里没有的时候会执行这个
   if [ ! -d $repo ]; then
   showMsg '---------- 警告: 你本地不存在文件夹 '$repo' 在你的本地仓库!准备从远端克隆...--------- '
   git clone ${gitlink}
   fi

   # 仅有一个空文件夹的时候会执行这个
   if [ "`ls -A $repo`" = "" ]; then
    showMsg '---------警告 : 文件夹 '$repo' 为空 !! 现在尝试从远端获取...-----------'
    rm -rf $repo
    git clone ${gitlink}
   fi 

   # 防止意外发生的安全判断
   if [ ! -d $repo ]; then
   showMsg '---------- 警告: 你仍然没有文件夹 '$repo' 在你的本地仓库! 请检查你拼接的git地址--------- '
   continue
   fi

   # 确认不为空且存在进入目录开始执行相关git操作
   cd $repo
   showMsg '---------- into '$repo' -----------'
   # qz=$repo
   #git checkout ./
   #git branch | grep -v "master" | xargs git branch -D
   #git checkout -b $repo$hz origin/master
   # 重置相关操作
   # if [ ! -n "$reset" ]; then
   #    showMsg '-----------Now clean Git repository--------'
   git clean -f
   git reset --hard HEAD
   # fi
   git fetch
   git checkout ${branch}
   git pull
   # git log
   #git commit ./ -m "牌墙"
   #git push origin $repo$hz
   #br=`git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3`
   #showMsg $br
   for file in ${listrm[@]}
   do
      if [ -f $file ]; then
      rm -rf ${file}
      showMsg '----------- 移除文件：'$file' ------------'
      fi
   done
   showMsg '-------- '$repo' 文件操作结束 ---------'
   cd ../
 done
 showMsg '------------拉取完成输入任意值结束日志查看状态----------'
 read anykey
 
 
 