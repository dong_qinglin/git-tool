# -*- coding: UTF-8 -*-

# 界面布局类

from sys import version, winver
from Tkinter import *
import Gamecontroller
import Packagecontroller


class GUI():
    def __init__(self,init_window_name):
        self.init_window_name = init_window_name
        self.package = dict()
        self.showlst =[]
        self.tarlst =dict()
        self.area = str()
        self.versionvar = IntVar()
        self.inputver = IntVar()
        Packagecontroller.VERSION=0
        Gamecontroller.readConfig()
        
    # 设置窗口
    def set_init_window(self):
        self.init_window_name.title("游戏打包工具_v1.0")    # 窗口名称
        # self.init_window_name.geometry('320*160+10+10')    # 290  160为窗口大小，+10+10定义窗口弹出时默认展示位置
        self.init_window_name.geometry('524x480+500+300')
        # self.init_window_name["bg"] = "pink"   # 窗体背景颜色，其他颜色见：blog.csdn.net/chl0000/article/details/7657887
        # self.init_window_name.attributes("-alpha",0.9)    # 虚化，值越小虚化程度越高
        # 展示区
        self.init_window_name.UIshow = LabelFrame(self.init_window_name)
        self.init_window_name.UIshow.grid(row=0,column=0)

        # 地区游戏
        self.init_window_name.showArea = LabelFrame(self.init_window_name.UIshow ,text='地区游戏')
        self.init_window_name.showArea.grid(row=0,column=0,padx=8,pady=6)

        # 可用操作按钮集
        self.init_window_name.buttonArea = LabelFrame(self.init_window_name.UIshow ,text='可用操作')
        self.init_window_name.buttonArea.grid(column=1, row=0)

        # 搜索功能
        self.init_window_name.searchArea = LabelFrame(self.init_window_name.buttonArea,text='地区搜索')
        self.init_window_name.searchArea.grid(row=0, column=0)

        # 列表操作
        self.init_window_name.operateList = LabelFrame(self.init_window_name.buttonArea,text="列表操作")
        self.init_window_name.operateList.grid(row=1, column=0)

        # 打包操作
        self.init_window_name.packOperate = LabelFrame(self.init_window_name.buttonArea,text="打包操作")
        self.init_window_name.packOperate.grid(row=2, column=0)


        # 结果集
        self.init_window_name.targetSet = LabelFrame(self.init_window_name.UIshow ,text='结果集')
        self.init_window_name.targetSet.grid(column=2,row=0,padx=8)

        # 注意事项
        self.init_window_name.warning = LabelFrame(self.init_window_name,text='使用方法：')
        self.init_window_name.warning.grid(column=0,row=2,padx=8)

        # 版权所有翻录必究
        self.init_window_name.copyright = Label(self.init_window_name,text="Copyright (C) 2021-2025, OpenCV Foundation, all rights reserved.")
        self.init_window_name.copyright.grid(column=0,row=3)


        # 标签用来警示
        self.init_window_name.warningLabel = Label(self.init_window_name.warning,text="首先，在输入框里输入地区，如：shanxi 然后，发起检索等待搜索结果显示在左侧区域，选择你要的游戏或者全部选择，单选需要点击游戏名称至变色，然后点击 > 按钮实现移动至结果集，你可以同时打不止一个地区，只需要再次键入地区名发起搜索即可。如需全部导入则点击 >> 按钮。选择完成后点击生成热更包即可",wraplength=500)
        self.init_window_name.warningLabel.pack(side=TOP,fill=X)


        # 输入框(搜索框，按钮也一并写在这)
        self.init_window_name.searchEntry = Text(self.init_window_name.searchArea,width = 15, height=1)
        self.init_window_name.searchEntry.grid(row=0,column=0)

        # 检索按钮，删除按钮,添加显示按钮
        self.init_window_name.search_btn = Button(self.init_window_name.searchArea,text='发起检索',command=lambda: self.showGame())
        self.init_window_name.search_btn.grid(row=1,column=0)


        # 移动到即将打包列表

        # 游戏列表，滚动条
        #垂直滚动条组件  
        self.init_window_name.tarScr = Scrollbar(self.init_window_name.targetSet)
        #设置垂直滚动条显示的位置 
        self.init_window_name.tarScr.pack(side=RIGHT,fill=Y)
        #Listbox组件添加Scrollbar组件的set()方法
        self.init_window_name.tar_lb = Listbox(self.init_window_name.targetSet,selectmode=SINGLE,yscrollcommand=self.init_window_name.tarScr.set,height=15)
        # self.lb.grid(row=0,column=1)
        self.init_window_name.tar_lb.pack(side=LEFT,fill=Y)
        #设置Scrollbar组件的command选项为该组件的yview()方法
        self.init_window_name.tarScr.config(command=self.init_window_name.tar_lb.yview)



        # 游戏列表，滚动条
        #垂直滚动条组件  
        self.init_window_name.slb = Scrollbar(self.init_window_name.showArea)
        #设置垂直滚动条显示的位置 
        self.init_window_name.slb.pack(side=RIGHT,fill=Y)
        #Listbox组件添加Scrollbar组件的set()方法
        self.init_window_name.lb = Listbox(self.init_window_name.showArea,selectmode=SINGLE,yscrollcommand=self.init_window_name.slb.set,height=15)
        # self.lb.grid(row=0,column=1)
        self.init_window_name.lb.pack(side=LEFT,fill=Y)
        #设置Scrollbar组件的command选项为该组件的yview()方法
        self.init_window_name.slb.config(command=self.init_window_name.lb.yview)

        # 删除列表项按钮
        self.init_window_name.bt1 = Button(self.init_window_name.operateList, text='<', command=lambda : self.moveBack(),width=4)
        self.init_window_name.bt1.grid(row=0,column=0,padx=8,columnspan=4)

        # 从左侧列表移除添加进结果集
        self.init_window_name.bt2 = Button(self.init_window_name.operateList, text='>', command=lambda: self.addToPackage(),width=4)
        self.init_window_name.bt2.grid(row=1,column=0,padx=8,columnspan=4)

        # 整个地区添加进结果集
        self.init_window_name.bt4 = Button(self.init_window_name.operateList, text='>>', command=lambda: self.addAllToPackage(),width=4)
        self.init_window_name.bt4.grid(row=2,column=0,padx=8,columnspan=4)


        # 生成热更包
        self.init_window_name.bt3 = Button(self.init_window_name.packOperate, text='生成热更包',command=lambda:self.packageNow())
        self.init_window_name.bt3.grid(row=0,column=0,padx=8,columnspan=4)

        # 打包模式选择
        # self.init_window_name.rd1 = Radiobutton(self.init_window_name.packOperate,text='直出热更包',variable =self.versionvar,value=0,command=self.printparam())
        # self.init_window_name.rd1.grid(row=1,column=0,padx=8)
        # self.init_window_name.rd1 = Radiobutton(self.init_window_name.packOperate,text='版本增一',variable = self.versionvar,value=1,command=self.printparam())
        # self.init_window_name.rd1.grid(row=2,column=0,padx=8)
        self.init_window_name.input_bt = Button(self.init_window_name.packOperate,text='手动直升版本',command=lambda:self.pleaseInputVersion())
        self.init_window_name.input_bt.grid(row=1,column=0,padx=8)

    # UI功能性操作

    # 由于是直接回车发起检索似乎不好用

    def showGame(self):
        src = self.init_window_name.searchEntry.get(1.0,END).strip().replace("\n","").encode()
        self.init_window_name.lb.delete(0,END)
        if src :
            self.init_window_name.searchEntry.delete(1.0,END)
            ls = Gamecontroller.getGameList(src)
            self.area = src
            self.showlst = ls
            print(self.showlst)
            print(self.area)
            length = len(ls)
            for i in range(length):
                self.init_window_name.lb.insert(END,ls[i])
    

    def addToPackage(self):
        index = self.init_window_name.lb.curselection()
        print(index)
        if index:
            self.init_window_name.lb.delete(index)        
            for i in index:
                item = self.showlst[i]
                del self.showlst[i]
                self.init_window_name.tar_lb.insert(END,item)
                self.tarlst[item] = self.area
                print(self.area)
                print(item)
                print(self.tarlst)
    
    def moveBack(self):
        index = self.init_window_name.tar_lb.curselection()
        print(index)
        if index:
            item = self.init_window_name.tar_lb.get(index)
            self.init_window_name.tar_lb.delete(index)
            for i in index:
                # item = self.tarlst[i][0]
                if self.tarlst[item] == self.area:
                    self.init_window_name.lb.insert(END,item)
                    self.showlst.append(item)
                    print(item)
                    print(self.tarlst)
                    print(self.showlst)
                else :
                    continue
                del self.tarlst[item]

    def packageNow(self):
        length = self.init_window_name.tar_lb.size()
        print(length)
        for i in range(length):
            item = self.init_window_name.tar_lb.get(i)
            area= self.tarlst[item]
            version = Gamecontroller.getGameVersion(area,item)
            # veradd = self.versionvar.get()
            version = version + 1
            Packagecontroller.packageorder(area,item,version)

    def printparam(self):
        print(self.versionvar)

    def addAllToPackage(self):
        length = len(self.showlst)
        self.init_window_name.lb.delete(0,END)
        for i in range(length):
            item = self.showlst[i]
            self.init_window_name.tar_lb.insert(END,item)
            self.tarlst[item] = self.area
            print(self.area)
            print(item)
            print(self.tarlst)
        self.showlst = []
        print(self.showlst)
    
    def pleaseInputVersion(self):
        # 显示在最上层top新窗体
        self.top = Toplevel()
        self.top.title('从零直升当前版本')
        self.top.geometry('+650+500')
        # self.init_window_name.protocol('WM_DELETE_WINDOW',self.activeButton())
        # self.init_window_name.bt3["state"] = DISABLED
        # self.init_window_name.input_bt["state"] = DISABLED
        self.inputver = Entry(self.top,width=10)
        self.inputver.grid(row=0,column=1,padx=1,pady=1)
        versionLabel = Label(self.top,text='请输入版本：')
        versionLabel.grid(row=0,column=0,padx=3,pady=1)
        confirm_bt = Button(self.top,text='生成',command=lambda:self.packageWithInput(),width=10)
        confirm_bt.grid(row=0,column=2,padx=5,pady=1)

    def packageWithInput(self):
        length = self.init_window_name.tar_lb.size()
        print(length)
        for i in range(length):
            item = self.init_window_name.tar_lb.get(i)
            area= self.tarlst[item]
            version = self.inputver.get()
            Packagecontroller.packageorder(area,item,version)
        self.top.destroy()
        # self.init_window_name.bt3["state"] = NORMAL
        # self.init_window_name.input_bt["state"] = NORMAL

    # def activeButton(self):
    #     self.init_window_name.bt3["state"] = NORMAL
    #     self.init_window_name.input_bt["state"] = NORMAL


def gui_start():
    init_window = Tk()          # 实例化出一个父窗口
    MY_PORTAL = GUI(init_window)
    # 设置窗口默认属性
    MY_PORTAL.set_init_window()

    init_window.mainloop()      # 父窗口进入事件循环


gui_start()
